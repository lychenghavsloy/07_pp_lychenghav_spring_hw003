package com.example.spring_hw003.Exception;

public class AuthorNotFound extends RuntimeException {

    public AuthorNotFound(String message){
        super(message);
    }
}
