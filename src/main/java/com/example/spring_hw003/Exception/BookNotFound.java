package com.example.spring_hw003.Exception;

public class BookNotFound extends RuntimeException {

    public BookNotFound(String message) {
        super(message);
    }
}