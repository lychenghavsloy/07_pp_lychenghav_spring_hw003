package com.example.spring_hw003.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandleException {

    @ExceptionHandler(AuthorNotFound.class)
    ProblemDetail authorNotFound(AuthorNotFound e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        return problemDetail;
    }

    @ExceptionHandler(CategoryNotFound.class)
    ProblemDetail categoryNotFound(CategoryNotFound a){
        ProblemDetail problemDetail1 = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, a.getMessage());
        return problemDetail1;
    }

    @ExceptionHandler(BookNotFound.class)
    ProblemDetail bookNotFound(BookNotFound d){
        ProblemDetail problemDetail1 = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, d.getMessage());
        return problemDetail1;
    }
}
