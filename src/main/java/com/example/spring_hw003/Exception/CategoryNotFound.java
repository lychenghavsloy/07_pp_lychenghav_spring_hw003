package com.example.spring_hw003.Exception;

public class CategoryNotFound extends RuntimeException{

    public CategoryNotFound(String message){
        super(message);
    }

}
