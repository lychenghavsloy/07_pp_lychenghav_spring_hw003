package com.example.spring_hw003.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.apache.ibatis.annotations.Mapper;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Mapper
public class Category {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;
    private String category_name;

}
