package com.example.spring_hw003.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Author {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;
    private String author_name;
    private String gender;

}
