package com.example.spring_hw003.Model.request;

import com.example.spring_hw003.Model.Author;
import com.example.spring_hw003.Model.Category;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Mapper
public class BookRequest {
    private String title;
    private LocalDateTime published_date;
    private Integer authorId;
    private List<Integer> categoryList;


}
