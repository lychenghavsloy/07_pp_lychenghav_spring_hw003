package com.example.spring_hw003.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Mapper
public class Book {
@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;
    private String title;
    private LocalDateTime published_date;
    private Author author;
    private List<Category> categoryList;


}
