package com.example.spring_hw003.Controller;

import com.example.spring_hw003.Model.Author;
import com.example.spring_hw003.Response.Entity;
import com.example.spring_hw003.Service.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/api/v1/get-all/Author")
    public ResponseEntity<Entity<ArrayList<Author>>> getAllAuthor (){
        Entity entity = new Entity();
        entity.setMessage("Successfully fetched authors");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(authorService.getAll());
        return ResponseEntity.ok().body(entity);
    }

    @GetMapping("/api/v1/get-author/id/{id}")
    public ResponseEntity<?> getAuthorByID (@PathVariable int id){
        Entity entity = new Entity();
        entity.setMessage("Fetched All Data Successfully");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(authorService.getAuthorByID(id));
        if(authorService.getAuthorByID(id) == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(entity);
    }

    @PostMapping("/api/v1/add-new-author")
    public ResponseEntity<?> newCustomer(@RequestBody Author author) {
        Entity entity = new Entity();
        entity.setMessage("Insert to Database Successfully");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(authorService.insertData(author));
        return ResponseEntity.ok().body(entity);
    }
    @PutMapping("/api/v1/update-author-by-id/{id}")
    public ResponseEntity<?> updateAuthorById(@RequestBody Author author ,@PathVariable Integer id) {
        Entity entity = new Entity();
        entity.setMessage("Update Successfully");
        entity.setStatus(HttpStatus.OK);
        authorService.updateAuthorByID(id,author);
        entity.setPayload(authorService.getAuthorByID(id));
        return ResponseEntity.ok().body(entity);
    }
    @DeleteMapping("/api/v1/delete-author-by-id/{id}")
    public ResponseEntity<?> deleteAuthorById(@PathVariable Integer id) {
        Entity entity = new Entity();
        entity.setMessage("Delete Successfully");
        entity.setStatus(HttpStatus.OK);
        authorService.deleteAuthorByID(id);
        return ResponseEntity.ok().body(entity);
    }
}
