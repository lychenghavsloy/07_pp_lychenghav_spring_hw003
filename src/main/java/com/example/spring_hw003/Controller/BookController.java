package com.example.spring_hw003.Controller;

import com.example.spring_hw003.Model.Author;
import com.example.spring_hw003.Model.Book;
import com.example.spring_hw003.Model.request.BookRequest;
import com.example.spring_hw003.Response.BookResponse;
import com.example.spring_hw003.Response.Entity;
import com.example.spring_hw003.Service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/api/v1/get-all/Book")
    public ResponseEntity<BookResponse<ArrayList<Book>>> getAllBook(){
        BookResponse entity = new BookResponse();
        entity.setTimestamp(LocalDateTime.now());
        entity.setMessage("Get book successfully");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(bookService.getAll());
        return ResponseEntity.ok().body(entity);
    }

    @GetMapping("/api/v1/get-book-by-id/{id}")
    public ResponseEntity<?> getBookById(@PathVariable Integer id){
        BookResponse entity = new BookResponse();
        entity.setTimestamp(LocalDateTime.now());
        entity.setMessage("Get book successful");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(bookService.getBookById(id));
        return ResponseEntity.ok().body(entity);
    }

    @PostMapping("/api/v1/insert-book/")
    public ResponseEntity<?> insertBook(@RequestBody BookRequest bookRequest){
        Integer id = bookService.insertBook(bookRequest);
        BookResponse entity = new BookResponse();
        entity.setTimestamp(LocalDateTime.now());
        entity.setMessage("New Book Insert");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(bookService.getBookById(id));
        return ResponseEntity.ok().body(entity);
    }
    @DeleteMapping("/api/v1/delete-book-by-id/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable Integer id){
        BookResponse entity = new BookResponse();
        entity.setTimestamp(LocalDateTime.now());
        entity.setMessage("Delete successfully");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(bookService.deleteBookById(id));
        return ResponseEntity.ok().body(entity);
    }

    @PutMapping("/api/v1/update-book-by-id/{id}")
    public ResponseEntity<?> updateBook(@PathVariable Integer id, @RequestBody BookRequest bookRequest){
        Integer iD = bookService.updateBook(id, bookRequest);
        BookResponse entity = new BookResponse();
        entity.setTimestamp(LocalDateTime.now());
        entity.setMessage("Delete successfully");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(bookService.getBookById(iD));
        return ResponseEntity.ok().body(entity);
    }
}
