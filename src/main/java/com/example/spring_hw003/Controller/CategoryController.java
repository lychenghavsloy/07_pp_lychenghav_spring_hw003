package com.example.spring_hw003.Controller;

import com.example.spring_hw003.Model.Author;
import com.example.spring_hw003.Model.Category;
import com.example.spring_hw003.Response.Entity;
import com.example.spring_hw003.Service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/api/v1/get-all/Category")
    public ResponseEntity<Entity<ArrayList<Category>>> getAllCategory (){
        Entity entity = new Entity();
        entity.setMessage("Successfully fetched authors");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(categoryService.getAllCategory());
        return ResponseEntity.ok().body(entity);
    }

    @GetMapping("/api/v1/get-category/id/{id}")
    public ResponseEntity<?> getCategoryByID (@PathVariable int id){
        Entity entity = new Entity();
        entity.setMessage("Fetched All Data Successfully");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(categoryService.getCategoryByID(id));
        if(categoryService.getCategoryByID(id) == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(entity);
    }

    @PostMapping("/api/v1/add-new-category")
    public ResponseEntity<?> newCategory(@RequestBody Category category) {
        Entity entity = new Entity();
        entity.setMessage("Insert to Database Successfully");
        entity.setStatus(HttpStatus.OK);
        entity.setPayload(categoryService.insertData(category));
        return ResponseEntity.ok().body(entity);
    }

    @PutMapping("/api/v1/update-category-by-id/{id}")
    public ResponseEntity<?> updateAuthorById(@RequestBody Category category ,@PathVariable Integer id) {
        Entity entity = new Entity();
        entity.setMessage("Update Successfully");
        entity.setStatus(HttpStatus.OK);
        categoryService.updateCategoryByID(id,category);
        entity.setPayload(categoryService.getCategoryByID(id));
        return ResponseEntity.ok().body(entity);
    }

    @DeleteMapping("/api/v1/delete-category-by-id/{id}")
    public ResponseEntity<?> deleteCategoryById(@PathVariable Integer id) {
        Entity entity = new Entity();
        entity.setMessage("Delete Successfully");
        entity.setStatus(HttpStatus.OK);
        categoryService.deleteCategoryByID(id);
        return ResponseEntity.ok().body(entity);
    }
}
