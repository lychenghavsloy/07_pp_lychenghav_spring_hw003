package com.example.spring_hw003.Repository;

import com.example.spring_hw003.Model.Book;
import com.example.spring_hw003.Model.request.BookRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
//@Repository
public interface BookRepository {

    @Select("""
            Select * from book
            """)
    @Results(id = "BookMap", value = {
            @Result(property = "id", column = "book_id"),
            @Result(property = "published_date", column = "publish_date"),
            @Result(property = "author", column = "author_id", one = @One(select = "com.example.spring_hw003.Repository.AuthorRepository.getAuthorById")),
            @Result(property = "categoryList", column = "book_id", many = @Many(select = "com.example.spring_hw003.Repository.CategoryRepository.getCategoryById"))
    })
//    @ResultMap("BookMap")
    List<Book> getAll();

    @Select("""
            SELECT * FROM book where book_id = #{id}
            """)
    @ResultMap("BookMap")
    Book getBookById(Integer id);

    @Select("DELETE FROM book where book_id = #{id}")
    @ResultMap("BookMap")
    Book deleteBookById(Integer id);

    @Select("INSERT INTO book(title, publish_date, author_id) VALUES (#{book.title}, #{book.published_date}, #{book.authorId}) RETURNING book_id")
//    @ResultMap("BookMap")
    Integer insertBook(@Param("book") BookRequest bookRequest);

    @Delete("DELETE FROM book_details WHERE book_id = #{id}")
    void deleteBookFromBookDetail(Integer id);

    @Select("UPDATE book SET title = #{book.title}, publish_date = #{book.published_date}, author_id = #{book.authorId} WHERE book_id = #{id} RETURNING book_id")
    Integer updateBook(Integer id,@Param("book") BookRequest bookRequest);

    @Insert("INSERT INTO book_details(book_id, category_id) VALUES(#{id}, #{id1})")
    void insertBookAndCategoryToBookDetail(Integer id, Integer id1);
}
