package com.example.spring_hw003.Repository;

import com.example.spring_hw003.Model.Author;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AuthorRepository {


    @Select("""
            SELECT * FROM author
            """)
    @Results(
            id = "AuthorMap", value = {
            @Result(property = "id", column = "author_id"),
            @Result(property = "author_name", column = "name"),
            @Result(property = "gender", column = "gender"),

    }
    )
     List<Author> getAll();


    @Select("""
            select * from author where author_id = #{id}
            """)
    @ResultMap("AuthorMap")
    Author getAuthorById(int id);


    @Select("""
            insert into author (name, gender)
            values(#{au.author_name}, #{au.gender}) returning *
            """)
    @ResultMap("AuthorMap")
    Author insertData(@Param("au") Author author);


    @Select("""
            update author set name= #{auID.author_name},gender= #{auID.gender} where author_id = #{id}
            """)
    @ResultMap("AuthorMap")
    Author updateAuthorByID(Integer id, @Param("auID") Author author);


    @Select("""
            DELETE FROM author WHERE author_id = #{id} returning *
            """)
    @ResultMap("AuthorMap")
    Author deleteAuthorByID(Integer id);
}
