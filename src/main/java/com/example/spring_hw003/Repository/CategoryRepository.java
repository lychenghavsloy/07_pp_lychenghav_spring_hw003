package com.example.spring_hw003.Repository;

import com.example.spring_hw003.Model.Category;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {


    @Select("""
            select * from category 
            """)
    @Results(
            id = "CategoryMap", value = {
            @Result(property = "id", column = "category_id"),
            @Result(property = "category_name", column = "name"),

    }
    )
    List<Category> getAllCategory();

    @Select("""
            select * from category where category_id = #{id}
            """)
    @ResultMap("CategoryMap")
    Category getCategoryByID(int id);

    @Select("""
            insert into category (name)
            values(#{Ca.category_name}) returning *
            """)
    @ResultMap("CategoryMap")
    Category insertData(@Param("Ca") Category category);

    @Select("""
            update category set name= #{up.category_name} where category_id = #{id}
            """)
    @ResultMap("CategoryMap")
    Category updateCategoryByID(@Param("up") Category category, Integer id);

    @Select("""
            DELETE FROM category WHERE category_id = #{id} returning *
            """)
    @ResultMap("CategoryMap")
    Category deleteCategoryByID(Integer id);

    @Select("SELECT p.category_id, name FROM category p JOIN book_details i on p.category_id = i.category_id where i.book_id = #{id}")
    @ResultMap("CategoryMap")
    List<Category> getCategoryById(Integer id);
}
