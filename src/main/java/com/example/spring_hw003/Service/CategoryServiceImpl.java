package com.example.spring_hw003.Service;

import com.example.spring_hw003.Exception.CategoryNotFound;
import com.example.spring_hw003.Model.Category;
import com.example.spring_hw003.Repository.CategoryRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
@Repository
@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.getAllCategory();
    }

    @Override
    public Category getCategoryByID(int id) {
        Category category = categoryRepository.getCategoryByID(id);
        if (category == null){
            throw new CategoryNotFound("This id "+ id + " not found");
        }
        return category;
    }

    @Override
    public Category insertData(Category category) {
        return categoryRepository.insertData(category);
    }

    @Override
    public Category updateCategoryByID(Integer id, Category category) {
        Category category1 = categoryRepository.updateCategoryByID(category,id);
        if (category1 == null) {
            throw new CategoryNotFound("This id "+ id + " not found");

        }
        return category1;
    }

    @Override
    public Category deleteCategoryByID(Integer id) {
        Category category = categoryRepository.deleteCategoryByID(id);
        if (category == null ) {
            throw new CategoryNotFound("This id "+ id + " not found");
        }
        return category;
    }
}
