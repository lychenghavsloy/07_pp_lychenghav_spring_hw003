package com.example.spring_hw003.Service;

import com.example.spring_hw003.Exception.AuthorNotFound;
import com.example.spring_hw003.Exception.BookNotFound;
import com.example.spring_hw003.Model.Book;
import com.example.spring_hw003.Model.request.BookRequest;
import com.example.spring_hw003.Repository.BookRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public class BookServiceImpl implements BookService{
    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.getAll();
    }

    @Override
    public Book getBookById(Integer id) {
        Book book =  bookRepository.getBookById(id);
        if (book == null) {
            throw new BookNotFound("This id "+ id + " not found");
        }
        return book;
    }

    @Override
    public Book deleteBookById(Integer id) {
        Book book =  bookRepository.deleteBookById(id);
        if (book == null) {
            throw new BookNotFound("This id "+ id + " not found");
        }
        return book;
    }

    @Override
    public Integer insertBook(BookRequest bookRequest) {
        Integer iD = bookRepository.insertBook(bookRequest);
        for (Integer Id: bookRequest.getCategoryList()){
            bookRepository.insertBookAndCategoryToBookDetail(iD, Id);
        }
        return iD;
    }

    @Override
    public Integer updateBook(Integer id, BookRequest bookRequest) {
        Integer iD = bookRepository.updateBook(id, bookRequest);
        bookRepository.deleteBookFromBookDetail(iD);
        for (Integer Id: bookRequest.getCategoryList()){
            bookRepository.insertBookAndCategoryToBookDetail(iD, Id);
        }
        return iD;
    }
}
