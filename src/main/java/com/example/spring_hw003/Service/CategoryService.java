package com.example.spring_hw003.Service;

import com.example.spring_hw003.Model.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAllCategory();


    Category getCategoryByID(int id);

    Category insertData(Category category);

    Category updateCategoryByID(Integer id, Category category);

    Category deleteCategoryByID(Integer id);
}
