package com.example.spring_hw003.Service;

import com.example.spring_hw003.Exception.AuthorNotFound;
import com.example.spring_hw003.Model.Author;
import com.example.spring_hw003.Repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService{
    private final AuthorRepository authorRepository;

    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public Author getAuthorByID(int id) {
        Author author = authorRepository.getAuthorById(id);
        if (author == null ){
            throw new AuthorNotFound("This id "+ id + " not found");
        }
        return author;
    }

    @Override
    public List<Author> getAll() {
        return authorRepository.getAll();
    }

    @Override
    public Author insertData(Author author) {
        return authorRepository.insertData(author);
    }

    @Override
    public Author updateAuthorByID(Integer id, Author author) {
        Author authorId = authorRepository.updateAuthorByID(id,author);
        if (authorId == null){
            throw new AuthorNotFound("This id "+ id + " not found");
        }
        return authorId;
    }

    @Override
    public Author deleteAuthorByID(Integer id) {
        Author author = authorRepository.deleteAuthorByID(id);
        if (author == null){
            throw new AuthorNotFound("This id "+ id + " not found");

        }
        return author;
    }
}
