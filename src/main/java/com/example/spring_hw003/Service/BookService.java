package com.example.spring_hw003.Service;

import com.example.spring_hw003.Model.Book;
import com.example.spring_hw003.Model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Book> getAll();

    Book getBookById(Integer id);

    Book deleteBookById(Integer id);

    Integer insertBook(BookRequest bookRequest);

    Integer updateBook(Integer id, BookRequest bookRequest);
}
