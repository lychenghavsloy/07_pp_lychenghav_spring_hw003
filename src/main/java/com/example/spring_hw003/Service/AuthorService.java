package com.example.spring_hw003.Service;

import com.example.spring_hw003.Model.Author;

import java.util.List;

public interface AuthorService {

    Author getAuthorByID(int id) ;


    List<Author> getAll();

    Author insertData(Author author);


    Author updateAuthorByID(Integer id, Author author);

    Author deleteAuthorByID(Integer id);
}
